import pywikibot.logging as logging
import pywikibot.comms.http as http
import re
from urllib.parse import urlencode, urljoin, unquote
from .util import canonicalise_name

# OpenStreetMap's schema is not very precisely defined.  This means
# that whether a Commons file is "in use on OpenStreetMap" is a bit
# uncertain.  I aim for something analagous to "in use on Wikipedia".
# The guideline is that a file is in use on OpenStreetMap if there is
# some application or Web site that will display the file based on
# data in OSM.  I also require that it display proper attribution or
# provide a link back to Commons.
#
# Known qualifying applications include:
#
# OpenStreetBrowser <https://openstreetbrowser.org/>, whose
# "Wikipedia" layer will display files referenced by the
# "wikimedia_commons" key.  It does not split on semicolons when
# displaying an image, though it does in its links in the left panel.
# It understands both commons.wikimedia.org URLs and page titles.
#
# Bexhill OpenStreetMap <https://bexhill-osm.org.uk/>, which displays
# images from lots of keys, all of them split on semicolons.  In each
# case, the numbered versions are only read if the un-numbered one is
# present:
#
#  - image, image:1, and image:2
#  - wikimedia_commons, wikimedia_commons:1, and wikimedia_commons:2
#  - wikimedia_commons:pano, wikimedia_commons:pano_1, and
#    wikimedia_commons:pano_2
#  - wikimedia_commons:video, wikimedia_commons:video_1,
#    wikimedia_commons:video_2
#
# Bexhill OpenStreetMap appears not to cope properly with full Commons
# URLs in the "image" tag.
#
# MapComplete <https://mapcomplete.org/>, whose WikimediaImageProvider
# defaults to looking at both wikimedia_commons and "image".  It
# splits values on semicolons and can handle several value formats.
# It handles URLs on both upload.wikimedia.org and
# commons.wikimedia.org, and also page titles.  For wikimedia_commons,
# it also supports titles without a namespace prefix.  It does prefix
# matching on keys, so it accepts wikimedia_commons:1 for instance.
#
# While this code could attempt to precisely follow the union of
# those, that would be quite confusing, so instead it does this:
#
# For each of wikimedia_commons, wikimedia_commons*, image, and image*:
#   For both the whole value and all semicolon-separated sub-values:
#     Recognize all of:
#       Commons file page URL
#       Commons image URL
#       Commons file page title
#
# It doesn't look for un-namespaced filenames in wikimedia_commons
# because they are very rare and would require different handling of
# different keys.
#
# The OpenStreetMap Wiki does document keys ending
# ":wikimedia_commons" to attach Commons files to particular aspects
# of a feature, but I haven't yet found any application that uses
# them.

class from_taginfo(dict):
    def __init__(self, baseurl="https://taginfo.openstreetmap.org"):
        self.baseurl = baseurl
        self.editsummary = self.get_editsummary()
        self.from_key_prefix("wikimedia_commons")
        self.from_key_prefix("image")
    def from_key_prefix(self, prefix):
        logging.info(f"Getting keys with prefix {prefix!r} from taginfo")
        r = http.fetch(urljoin(self.baseurl, "api/4/keys/all"),
                       params={'query': prefix})
        r.raise_for_status()
        j = r.json()
        logging.info(f"{len(j['data'])} possible keys found")
        for keyinfo in j["data"]:
            if keyinfo["key"].startswith(prefix):
                self.from_key(keyinfo["key"])
    def from_key(self, key):
        logging.info(f"Getting values of {key!r}")
        page = 1
        rp = 999
        while True:
            r = http.fetch(urljoin(self.baseurl, "api/4/key/values"),
                           params={'key': key, 'page': page, 'rp': rp})
            r.raise_for_status()
            j = r.json()
            if j["data"] == []: break
            page += 1
            for v in j["data"]:
                titles = [v["value"]]
                if ";" in v["value"]: titles += v["value"].split(";")
                for title in titles:
                    # image=* might be either a Commons page title or a URL
                    if m := re.match("^https?://commons.wikimedia.org/wiki/"
                                     "(File:[^#?]*)",
                                     title, re.IGNORECASE):
                        title = m[1]
                    elif m := re.match("^https?://upload.wikimedia.org/"
                                       "wikipedia/commons/./../(.*)$",
                                       title, re.IGNORECASE):
                        title = f"File:{m[1]}"
                    if re.match("^File:", title, re.IGNORECASE):
                        params = urlencode(dict(key=key, value=v['value']))
                        tiurl = urljoin(self.baseurl, "tags/?" + params)
                        title = canonicalise_name(title)
                        if title in self:
                            self[title] += "<br/>"
                        else:
                            self[title] = ""
                        self[title] += (f"[{tiurl} {key}={v['value']}]")
    def get_editsummary(self):
        r = http.fetch(urljoin(self.baseurl, "api/4/site/info"))
        r.raise_for_status()
        site_info = r.json()
        r = http.fetch(urljoin(self.baseurl, "api/4/site/sources"))
        r.raise_for_status()
        site_sources = r.json()
        dbsrc = [src for src in site_sources if src['id'] == 'db'][0]
        return (f"; data via {site_info['name']} [{site_info['url']}]; "
                f"correct as of {dbsrc['data_until']} UTC")


oplq = """
[out:json];
// gather results
(
  node["wikimedia_commons"~"(^|;)File:",i];
  way["wikimedia_commons"~"(^|;)^File:",i];
  relation["wikimedia_commons"~"(^|;)File:",i];
);
// print results
out tags qt;
"""

def from_overpass():
    r = http.fetch("https://overpass-api.de/api/interpreter",
                   params={'data': oplq})
    r.raise_for_status()
    j = r.json()
    files = {}

    for e in j["elements"]:
        wc = e.get("tags", {}).get("wikimedia_commons")
        if wc != None:
            osm_type = e["type"]
            osm_id = e["id"]
            files[wc] = (f"[https://www.openstreetmap.org/{osm_type}/{osm_id} "
                         f"{osm_type} {osm_id}]")
    return files

