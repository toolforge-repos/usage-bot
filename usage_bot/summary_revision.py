import pywikibot.data.api as api
from subprocess import run, CalledProcessError, PIPE

def gitlab_prefix(site):
    # Find out if this wiki has an interwiki link to Wikimedia GitLab
    r = api.Request(site, parameters=dict(action='query', meta='siteinfo',
                                          siprop='interwikimap'))
    for iw in r.submit()['query']['interwikimap']:
        if iw['url'] == 'https://gitlab.wikimedia.org/$1':
            return iw['prefix']
    return None

def summary_revision(site):
    # Return a suitable suffix for an edit summary indicating which
    # code revision we're running.
    try:
        gitrev = run(['git', 'rev-parse', 'HEAD'],
                     check=True, encoding='utf-8', stdout=PIPE).stdout.rstrip()
        prefix = gitlab_prefix(site)
        if prefix == None:
            return f" | {gitrev[:7]}"
        return (
            f" | [[{prefix}:toolforge-repos/usage-bot/-/tree/{gitrev}|"
            f"{gitrev[:7]}]]")
    except CalledProcessError:
        return ""
