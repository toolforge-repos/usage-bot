"""
This family file was auto-generated by generate_family_file.py script.

Configuration parameters:
  url = https://wikiconference.org/wiki/2024/Main_Page
  name = wcna

Please do not commit this to the Git repository!
"""
from pywikibot import family


class Family(family.Family):  # noqa: D101

    name = 'wcna'
    langs = {
        'en': 'wikiconference.org',
    }

    def scriptpath(self, code):
        return {
            'en': '',
        }[code]

    def protocol(self, code):
        return {
            'en': 'https',
        }[code]
